// Module Dependencies
var Strategy = require('./strategy');

// Export the strategy from the package
exports = module.exports = Strategy;

// Export the strategy's constructors
exports.Strategy = Strategy;
