// Strategy Dependencies
var passport = require('passport-strategy');
var util = require('util');
var request = require('request');

// Create the strategy
function Strategy (options, verify) {
  // Require that we have a callback function
  if (typeof options == 'function') {
    verify = options;
    options = undefined;
  }

  options = options || {};

  if (!verify) {
    throw new Error('SAML authentication strategy requires a verify function');
  }

  passport.Strategy.call(this);
  this.name = 'juxtaprint';
  this._verify = verify;
  this._passReqToCallback = options.passReqToCallback;
  this._apikey = options.apikey;
	this._customAPIURL = options.customAPIURL;
  this._callbackURL = options.callbackURL || '';
}

// Have the new strategy inherit the base strategy
util.inherits(Strategy, passport.Strategy);

// Prototype the function for when the dev calls "passport.authenticate"
Strategy.prototype.authenticate = function (req, options) {
  options = options || {};
  var self = this;

  // Set the options from the request
  var apikey = options.apikey || this._apikey;
  var apiURL = options.customAPIURL || this._customAPIURL || 'http://api.juxttech.com/v1/juxtaprint';
  var loginPageURL = options.loginPageURL || 'http://account.juxttech.com';
  var callbackURL = options.callbackURL || this._callbackURL;
  var username = req.body.username;
  var password = req.body.password;

  // Does our POST body contain Juxtaprint data?
  // If it doesn't, let's redirect the user to the Juxtaprint Login page (or a custom page)
  if (!username) {
    return this.redirect(loginPageURL + '?callbackURL=' + encodeURIComponent(callbackURL));
  }
  else {
    var loginRequest = function() {
      // Send a post request to the Juxtaprint API
      request.post({
        headers: {'content-type' : 'application/x-www-form-urlencoded'},
        url: apiURL + '/authenticate',
        body: 'apikey=' + apikey + '&username=' + username + '&password=' + password
      }, function optionalCallback(err, httpResponse, body) {
        // Validate the Request
        if (err) {
          return self.fail('Unable to contact the Juxtaprint API!');
        }
        else {
          // Let's check for juxtaprint errors before continuing
          var serverResponse = JSON.parse(body);
          var success = serverResponse.success;
          if (success === true) {
            try {
              if (self._passReqToCallback) {
                self._verify(req, body, verified);
              } else {
                self._verify(body, verified);
              }
            } catch (ex) {
              return self.error(ex);
            }
          }
          else {
            // Looks like something went wrong, but it's probably the developer's fault
            self.fail('An Unknown Error has Occurred');
          }
        }
      });
    }

    // Run a verification function
    function verified(err, user, info) {
      if (err) { return self.error(err); }
      if (!user) { return self.fail(info); }
      self.success(user, info);
    }
  }

  loginRequest();
}

// Export the Strategy
module.exports = Strategy;
