# passport-juxtaprint
General-purpose [Juxtaprint](http://account.juxttech.com) Strategy for [Passport](http://passportjs.org/)

This module lets you authenticate using Juxtaprint in your node.js applications.
By plugging this module into passport, you can make calls to the juxtaprint API with nothing but a few lines of code and an API key.

## License
Copyright (C) 2016 Juxtapose Technologies, All Rights Reserved

The Juxtaprint Passport Strategy is licensed under the MIT License. See LICENSE for more information.

## Prerequisites
* Node.js Version 6.9.2 LTS
* Node Package Manager (NPM) Version 3.10.9 or higher

## Installation
run the following command
```
npm install https://gitlab.com/juxttech/passport-juxtaprint-koa.git
```

## Usage
### Strategy Configuration
Not much configuration is required for Juxtaprint other than the API key, however there are a few other options available.
```
var JuxtaprintStrategy = require('passport-juxtaprint').Strategy;
passport.use(new JuxtaprintStrategy({
    apikey: 'JUXTAPRINT_API_KEY',
    callbackURL: 'http://localhost:3000/login'
  }, function(user, done) {
    return done(null, user);
  })
);
```

The other options for configuring Juxtaprint are as follows:
* `customAPIURL: 'http://example.com/v1/juxtaprint'` -- This option is available if you're using an external, non Juxtapose hosted Juxtaprint API
* `loginPageURL: 'http://example.com/login'` -- This option allows users to use a custom login page for Juxtaprint, rather than the one provided by Juxtapose

### Authenticating Requests
Use passport.authenticate(), specifying the `juxtaprint` strategy, to authenticate requests.

Here is the correct way to use Juxtaprint in a Koa application
```
app.use('/login',
  passport.authenticate('juxtaprint', {
    failureRedirect: '/login'
  }),
  function *(next) {
    this.redirect('/');
  }
);
```
